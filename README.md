____QUADTREE____ by Pierre Guerbeau--Gicquel
<br><br>
A quadtree or quaternary tree (Q tree) is a tree-like data structure in which each node has four wires.
<br>Quadtrees are most often used to partition a two-dimensional space by recursively subdividing
it into four nodes.
<br><br>
There are several types of quadtree. In our case it is a quadtree "region".
The quadtree «region» represents a partition of space in two dimensions by decomposing the region into four <br>
equal quadrants, then each quadrant into four sub-quadrants, and so on, with each «terminal node» comprising<br>
data corresponding to a sub-quadrantspecific region. Each node of the tree has exactly: either four children,<br>
or none (case of a terminal node).
Each node has four elements. This is a known technique for encoding images.  To simplify, the images are square,<br>
black and white and side 2^n.
<br><br>
Every file is explained  for the comprehension of the repository
<br><br>
__quadtree.py :__
<br><br>
This file contain all the functions, moduls and class needed for creation of a quadtree.
With the import of the file of param from src, it can create an image of width maximum and with the color wanted.
<br>With the import of Frame and Tk from Tkinter, it can create graphics interfaces.
<br>Annotations from future is for the documentation and annotation of code.
<br>And finally the import of json is for transform a file in a list.
<br>We create a class Quadtree with multiples functions for the creation of the Quadtree and his test.
<br>The constant NB_NODES give the number of nodes that is  4.
<br>The function init is for the initialization of the quadtree with the parameters of the 4 squares
(top left, top right, down right, down left) that will be called blocks.
<br>The function depht is here for calculate the depth of the quadtree
<br>fromFile is here to open the file wanted and transform it in list with json's modul.
<br>fromList verify if every element of a list in the file is a list or a bool and if it's a list create
a new Quadtree and do it until there's no new list.
<br><br>
__test_quadtree.py:__
<br>The moduls imported in this file (os and sys) are used for use the path for the testing of the different
file wanted.
<br>We import the class QuadTree and TkQuadTree in this file for link all the parameters of this class
with the tests.
<br>The function test_sample is for the test of the file quadtree.txt.
<br>The function test_single is for the test of the file quadtree_easy.txt.
<br><br>
__param.py:__
<br>The constant MAX_SIZE is for the size maximum of the picture wanted with Tkinter.
<br>The variable color_dict is for the colors used with Tkinter.
<br><br>
__quadtree.txt__
<br> Txt file of a quadtree of four's depth.
<br><br>
__quadtree_easy.txt:__
<br>Txt file of a quadtree of one's depth.
    