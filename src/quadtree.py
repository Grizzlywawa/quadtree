from __future__ import annotations

import json
from tkinter import Tk, Frame

from src.param import *


class QuadTree:
    NB_NODES: int = 4

    def __init__(self, hg: bool | QuadTree, hd: bool | QuadTree, bd: bool | QuadTree, bg: bool | QuadTree):
        """creation of a block with hg, hd, bd, bg in the Quadtree"""
        self.__blocks = [hg, hd, bd, bg]

    @property
    def depth(self) -> int:
        """ Recursion depth of the quadtree
        Verify every block in quadtree and if  block's depth is superior of max_depth"""
        max_depth = 0

        for block in self.__blocks:
            if isinstance(block, QuadTree) and block.depth > max_depth:
                max_depth = block.depth

        return max_depth + 1

    @staticmethod
    def fromFile(filename: str) -> QuadTree:
        """ Open a given file, containing a textual representation of a list
        try to read a file based on his filename et transform the file in a list that's
        put in function fromList with an exception in case of an error"""
        try:
            with open(filename, 'r') as file:
                lst = json.load(file)
                return QuadTree.fromList(lst)
        except Exception as e:
            print("fromfile() error" + str(e))

    @staticmethod
    def fromList(qt_list: list) -> QuadTree:
        """ Generates a Quadtree from a list representation
        create a new quadtree if the element in qt_list is a list and if not put juste the element (leaf)"""
        qt_settings = []
        for element in qt_list:
            if isinstance(element, list):
                qt_settings.append(QuadTree.fromList(element))
            else:
                qt_settings.append(element)
        return QuadTree(qt_settings[0], qt_settings[1], qt_settings[2], qt_settings[3])

    @property
    def blocks(self):
        """return blocks property"""
        return self.__blocks


class TkQuadTree(Tk):
    COORD_X: list = [0, 1, 0, 1]
    COORD_Y: list = [0, 0, 1, 1]

    def paint(self):
        """ TK representation of a Quadtree"""
        pass

    def newQuadtreeFrame(self: Frame, qt: QuadTree, depth: int = 0):
        depth += 1
        length = MAX_SIZE // (2 ** depth)
        for index, element in enumerate(qt.blocks):
            x_pos = self.winfo_x() + (TkQuadTree.COORD_X[index] * length)
            y_pos = self.winfo_y() + (TkQuadTree.COORD_Y[index] * length)
            if isinstance(element, QuadTree):
                frame = Frame(self, width=length, height=length)
                frame.place(x=x_pos, y=y_pos)
                TkQuadTree.newQuadtreeFrame(frame, element, depth)
            else:
                frame = Frame(self, bg=color_dict[element], width=length, height=length)
                frame.place(x=x_pos, y=y_pos)

    def __init__(self, filename: str):
        super().__init__()
        self.__quadtree = QuadTree.fromFile(filename)
        self.geometry(f"{MAX_SIZE}x{MAX_SIZE}")
        self.newQuadtreeFrame(self, self.__quadtree)
        self.title(f"{filename} | Depth : {str(self.__quadtree.depth)} layer(s)")
