"""size maximum wanted for the creation of the quadtree with Tkinter"""
MAX_SIZE = 512

"""colors of the quadtree wanted for his creation in Tkinter """
color_dict = {1: "black", 0: "white"}