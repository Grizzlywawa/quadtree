import os
import sys

sys.path.append(os.path.realpath(os.path.dirname(__file__) + "/.."))
from src import QuadTree


def test_sample():
    """test of the Quadtree's depth of the file quadtree.txt with fromFile's function with a result of 4 """
    filename = "../files/quadtree.txt"
    q = QuadTree.fromFile(filename)
    assert q.depth == 4


def test_single():
    """test of the Quadtree's depth of the file quadtree_easy.txt with fromFile's function with a result of 1  """
    filename = "../files/quadtree_easy.txt"
    q = QuadTree.fromFile(filename)
    assert q.depth == 1
